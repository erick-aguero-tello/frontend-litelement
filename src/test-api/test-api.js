import { LitElement, html } from 'lit-element';
class TestApi extends LitElement {
    
    static get properties() {
        return {
            movies: {type: Array}
        };
    }

    constructor() {
        super();
        this.movies = []; 
        this.getMovieData();
    }
    
    render() {
        return html `
            <br/>
            <hr>
            <h1>Título de películas:</h1>
            ${this.movies.map(
                movie => html `
                        <div style="background-color: aliceblue;border: 1px solid #f5f0f0;padding: 1rem;">
                            La pelicula ${movie.title}, fue dirigida por ${movie.director}
                        </div> `
            )}
            <br/>
            <br/>
        `;
    }


    async getMovieData(e) {
        console.log("getMovieData");
        console.log("Obteniendo datos de los filmes de SW");
        
        /*let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                this.movies = APIResponse.results;
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films/");
        xhr.send();*/

        //Use API fetch Javascript
        //Use promise
        this.getApiResponseMovie().then((result) => {
            this.movies = result.results;

        }, (error) => {
            console.log("error " + error);
            this.movies = [];
        });
    }


    async getApiResponseMovie() {
        try {
            let reqHeader = new Headers();
            reqHeader.append('Content-Type', 'text/json');
        
            /*let initObject = {
                method: 'GET', headers: reqHeader,
            };*/

            let initObject = {
                method: 'GET'
            };
            let result = await fetch('https://swapi.dev/api/films/', initObject);
            return result.json();
            

            /*
            fetch('https://swapi.dev/api/films/', initObject)
            .then(function (response) {
                console.log(response);
                return response.json();
            })
            .then(function (data) {
                console.log(data.results);
                return data.results;
            })
            .catch(function (err) {
                console.log("Something went wrong!", err);
            });
            
            */
        } catch (error) {
            throw error;
        }
    }

}
customElements.define('test-api', TestApi)