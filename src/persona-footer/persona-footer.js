import { LitElement, html } from 'lit-element';

class PersonaFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html `
            <h1>@PersonaApp 2020</h1>
        `;
    }
}
customElements.define('persona-footer', PersonaFooter)