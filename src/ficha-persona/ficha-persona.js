import { LitElement, html } from 'lit-element';
class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: { type: String},
            photo: {type: Object}
        };
    }

    constructor() {
        super();
        //Inicializar properties
        this.name = "Erick Agüero";
        this.yearsInCompany = 28;
        this.photo = {
            src: "./img/persona.jpg",
            alt: "foto persona"
        };

        this.updatePersonInfo();
    };

    updated(changeProperties) {
        if(changeProperties.has("name")) {
            console.log('Propiedad name Cambio su valor de '+ changeProperties.get("name") + " a "+ this.name);    
        }

        if(changeProperties.has("yearsInCompany")) {
            console.log('Propiedad yearsInCompany cambio su valor de '+ changeProperties.get("yearsInCompany") + " a "+ this.yearsInCompany);    
            this.updatePersonInfo();
        }

        if(changeProperties.has("personInfo")) {
            console.log('Propiedad personInfo cambio su valor de '+ changeProperties.get("personInfo") + " a "+ this.personInfo);    
        }
    }

    updateName(e) {
        console.log('updateName');
        this.name = e.target.value;
    }

    updatePersonInfo() {
        console.log('updatePersonInfo');
        console.log('yearsInCompany vale '+TouchList.yearsInCompany);

        if(this.yearsInCompany >= 7) {
            this.personInfo = "lead";
        } else if(this.yearsInCompany >= 5) {
            this.personInfo = "senior";
        } else if(this.yearsInCompany > 3) {
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        }
    }

    updateYearsInCompany(e) {
        console.log('updateYearsInCompany');
        this.yearsInCompany = e.target.value
    }

    render() {
        return html `
            <div>Ficha persona</div>
            <div>
                <label for="fname">Nombre completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}" />
                <br/>

                <label for="yearsInCompany">Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}" />
                <br/>

                <label for="personInfo">Puesto</label>
                <input type="text" name="personInfo" value="${this.personInfo}" disabled />
                <br/>

                <img src="${this.photo.src}" height="200" witdh="200" alt="${this.photo.alt}" />
            </div>
        `;
    }

}
customElements.define('ficha-persona', FichaPersona)