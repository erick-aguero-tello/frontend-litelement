import { LitElement, html, css } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {


    static get styles() {
        return css `
            :host {
                all: initial;
            }
        `;
    }

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.people = [
            {
                profile: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'name': 'Erick',
                'yearInCompany': 10,
                'photo': {
                    'src' : './img/persona.jpg',
                    'alt': 'Erick'
                }
            },
            {
                profile: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'name': 'Wester',
                'yearInCompany': 11,
                'photo': {
                    'src' : './img/persona.jpg',
                    'alt': 'Wester'
                }
            },
            {
                profile: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'name': 'Prueba 1',
                'yearInCompany': 12,
                'photo': {
                    'src' : './img/persona.jpg',
                    'alt': 'Prueba 1'
                }
            },
            {
                profile: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'name': 'Leandro Gao',
                'yearInCompany': 21,
                'photo': {
                    'src' : './img/persona.jpg',
                    'alt': 'Leandro Gao'
                }
            },
            {
                profile: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'name': 'Edgar Ballesteros',
                'yearInCompany': 3,
                'photo': {
                    'src' : './img/persona.jpg',
                    'alt': 'Edgar Ballesteros'
                }
            }
        ];
        this.showPersonForm = false;
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            
            <h2 class="text-center">Personas</h2>
            
            <main>
                <div class="row" id="peopleList">
                    <div class="row row-cols-1 row-cols-sm-4">
                        ${this.people.map(
                            person => html`<persona-ficha-listado 
                            name="${person.name}" 
                            yearsInCompany="${person.yearInCompany}"
                            profile="${person.profile}"
                            .photo="${person.photo}"
                            @delete-person="${this.deletePerson}"  
                            @info-person="${this.infoPerson}"
                            >
                            </persona-ficha-listado>`
                        )}
                    </div>
                </div>

                <div class="row">
                    <persona-form 
                     class="d-none border rounded border-primary"
                     id="personaForm"
                     @person-form-close="${this.personFormClose}"
                     @person-form-store="${this.personFormStore}"></persona-form>
                </div>

            </main>
        `;
    }

    deletePerson(e) {
        console.log("deletePerson persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e) {
        console.log("infoPerson Main ");
        console.log("Se ha solicitado más Información de la persona " + e.detail.name)

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        this.shadowRoot.getElementById("personaForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("personaForm").editingPerson = true;
        this.showPersonForm = true;

    }

    updated(changedProperties) {
        console.log("updated");
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de showPersonForm en persona-main");
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if(changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main");
            this.dispatchEvent(new CustomEvent("update-people", {
                detail: {
                    people: this.people
                }
            }));
        }
    }


    showPersonList() {
        console.log("showPersonList");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personaForm").classList.add("d-none");
    }


    showPersonFormData() {
        console.log("showPersonFormData");
        this.shadowRoot.getElementById("personaForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }


    personFormClose() {
        console.log("personFormClose");
        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log("personFormStore en persona-main");
        console.log(e.detail.person);

        if (e.detail.editingPerson === true) {
            console.log("Se va actualizar la persona de nombre " + e.detail.name);
            
            this.people = this.people.map(
                person => person.name === e.detail.person
                ? person = e.detail.person : person);
            
            let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.name
            );

            if (indexOfPerson >= 0) {
                console.log("Persona encontrada");
                this.people[indexOfPerson] = e.detail;
            }

        } else {
            console.log("Se va almacenar una persona nueva");
            this.people.push(e.detail);
        }

        console.log("Proceso terminado");
        console.log(this.people);
        this.showPersonForm = false;
    }
}
customElements.define('persona-main', PersonaMain)