import { LitElement, html } from 'lit-element';
class EmisorEvento extends LitElement {
    
    static get properties() {
        return {

        };
    }

    constructor() {
        super();
    }

    render() {
        return html `
            <div>Evento emisor</div>
            <button @click="${this.sendEvent}">No pulsar</button>
        `;
    }

    sendEvent(e) {
        console.log('Se ha pulasdo el botón');

        this.dispatchEvent(new CustomEvent("test-event", {
           "detail": {
               "course": "TechU",
               "year": 2020
           }     
        })
    )
    }
}
customElements.define('emisor-evento', EmisorEvento)