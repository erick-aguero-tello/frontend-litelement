import { LitElement, html, css } from 'lit-element';

class TestBootstrap extends LitElement {

    static get styles() {
        return css `
            .redbg {
                background-color: red;
            }
            .greenbg {
                background-color: green;
            }
            .bluebg {
                background-color: blue;
            }
            .greybg {
                background-color: silver;
            }
        `;
    }

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html `
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
            <h3>Probando Bootstrap!</h3>
            <div class="row greybg">
                <div class="col-2 redbg offset-1">Col 1</div>
                <div class="col-3 greenbg offset-2">Col 2</div>
                <div class="col-4 bluebg">Col 3</div>
            </div>
        `;
    }
}
customElements.define('test-bootstrap', TestBootstrap)